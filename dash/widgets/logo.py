import math
import color

def fri3d(border=2, col=color.BLUE, dotted=False, size=1):
	def render(cell):	
		cx = int(math.floor(cell.w / 2))
		cy = int(math.floor(cell.h / 2))

		# -- bottom
		x0 = cx
		y0 = cell.h - border

		# -- left
		x1 = border
		y1 = cy

		# -- right
		x2 = cell.w - border
		y2 = cy

		# -- top left
		x3 = int(math.floor(cx / 4))
		y3 = border

		# -- top right
		x4 = cell.w - int(math.floor(cx / 4))
		y4 = border

		cell.line(x0, y0, x1, y1, col=col, dotted=dotted, size=size)
		cell.line(x0, y0, x2, y2, col=col, dotted=dotted, size=size)
		cell.line(x0, y0, x3, y3, col=col, dotted=dotted, size=size)
		cell.line(x0, y0, x4, y4, col=col, dotted=dotted, size=size)

		cell.line(x1, y1, x4, y4, col=col, dotted=dotted, size=size)
		cell.line(x2, y2, x3, y3, col=col, dotted=dotted, size=size)

	return render