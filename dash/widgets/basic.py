def color(col):
    def render(cell):
        cell.rect(0, 0, cell.w, cell.h, col=col)

    return render

def text(txt, x=0, y=0):
    def render(cell):
        cell.print(txt, posx=x, posy=y)

    return render