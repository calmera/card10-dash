import utime
import math

CHAR_WIDTH=14
CHAR_HEIGHT=20

def digital(include_seconds=True):
	def render(cell):
		localtime = utime.localtime()
		hour = localtime[3]
		minute = localtime[4]

		y = int(math.floor((cell.h - CHAR_HEIGHT) / 2))

		if include_seconds:
			seconds = localtime[5]

			x = int(math.floor((cell.w - (8 * CHAR_WIDTH)) / 2))

			cell.print("{:02}:{:02}:{:02}".format(hour, minute, seconds), x=x, y=y)
		else:
			x = int(math.floor((cell.w - (5 * CHAR_WIDTH)) / 2))

			cell.print("{:02}:{:02}".format(hour, minute), x=x, y=y)

	return render