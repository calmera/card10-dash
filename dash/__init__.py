import utime
import vibra
import display
import os
import color as col

from dash.stage import Stage
from dash.widgets.basic import text, color
from dash.widgets.clock import digital as digital_clock
from dash.widgets.logo import fri3d as fri3d_logo

with display.open() as disp:
    s = Stage(disp)
    s.add("top", 0, 0, 160, 20)

    s.add("central_logo",     0, 20,  60, 60)
    s.add("central_content", 60, 20, 100, 60)

    # s.attach("top", color(col.RED))
    s.attach("central_logo", fri3d_logo())
    s.attach("central_content", digital_clock(include_seconds=False))

    while True:
        s.render()
