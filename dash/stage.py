import math

class Stage:
    def __init__(self, display):
        self.cells = {}
        self.display = display

    def add(self, name, x, y, w, h):
        self.cells[name] = Cell(self.display, name, x, y, w, h)

    def attach(self, name, renderer):
        cell = self.cells[name]
        if cell != None:
            cell.renderer = renderer

    def render(self):
        self.display.clear()

        # self.display.print("{:02}:{:02}:{:02}".format(hour, minute, seconds), posx=22, posy=33)

        for name, cell in self.cells.items():
            if cell.renderer != None:
                cell.renderer(cell)

        self.display.update()
        

class Cell:
    def __init__(self, display, name, x, y, w, h):
        self.display = display
        self.name = name
        self.x = x
        self.y = y
        self.w = w
        self.h = h
        self.renderer = None

        print("CELL '%s' (%d, %d, %d, %d)" % (self.name, self.x, self.y, self.w, self.h))

    def attach(self, renderer):
        self.renderer = renderer

    def print(self, text, fg=None, bg=None, x=0, y=0):
        x = self.x + x
        y = self.y + y

        self.display.print(text, fg=fg, bg=bg, posx=x, posy=y)

    def pixel(self, x, y, col=None):
        x = self.x + x
        y = self.y + y

        self.display.pixel(x, y, col=col)

    def line(self, xs, ys, xe, ye, col=None, dotted=False, size=1):
        xs = self.x + xs
        xe = self.x + xe
        ys = self.y + ys
        ye = self.y + ye

        self.display.line(xs, ys, xe, ye, col=col, dotted=dotted, size=size)

    def rect(self, xs, ys, xe, ye, col=None, filled=True, size=1):    
        xs = self.x + xs
        xe = self.x + xe
        ys = self.y + ys
        ye = self.y + ye

        self.display.rect(xs, ys, xe, ye, col=col, filled=filled, size=size)

    def circ(self, x, y, rad, col=None, filled=True, size=1):
        x = self.x + x
        y = self.y + y

        self.display.circ(x, y, rad, col=col, filled=filled, size=size)